import React, { Component, Fragment } from 'react';
import {Switch, Route, NavLink} from "react-router-dom";
import Home from "./components/Home/Home";
import Contact from "./components/Contact/Contact";
import About from "./components/About/About";

class App extends Component {
  render() {
    return (
        <Fragment>
            <nav className="navbar navbar-toggleable-md navbar-light bg-faded">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"/>
                </button>
                <a className="navbar-brand" href="#">Bi-Travel</a>
              <div className="collapse navbar-collapse" id="navbarNavDropdown">
                <ul className="navbar-nav">
                  <li className="nav-item">
                    <NavLink class="nav-link" to="/">Home</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink class="nav-link" to="/about">About</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink class="nav-link" to="/contacts">Contacts</NavLink>
                  </li>
                </ul>
              </div>
          </nav>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/about" exact component={About}/>
            <Route path="/contacts" exact component={Contact} />
          </Switch>
        </Fragment>
    );
  }
}

export default App;
