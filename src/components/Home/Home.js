import React from 'react';
import './Home.css';

const Home = props => {
  return (
      <div className="Home">
          <div class="container">

              <div class="jumbotron">
                  <h1>Travel around the world!</h1>
                  <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet.</p>
                  <p><a class="btn btn-lg btn-success" href="#" role="button">Book tickets now</a></p>
              </div>

          </div>
      </div>
  );
};

export default Home;