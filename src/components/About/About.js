import React from 'react';
import './About.css';

const About = props => {
    return (
        <div className="About">
            <div class="row">
                <div class="col-lg-4">
                    <h2>20% Discount now!</h2>
                    <p>Vestibulum pellentesque rhoncus nunc eu bibendum. Fusce gravida consequat leo, vel pulvinar augue volutpat non.</p>
                    <p><a class="btn btn-primary" href="#" role="button">View details »</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Travel in comfort</h2>
                    <p>Nulla semper, augue sit amet maximus aliquet, sapien nisl suscipit urna, vitae mattis lectus eros sed justo. </p>
                    <p><a class="btn btn-primary" href="#" role="button">View details »</a></p>
                </div>
                <div class="col-lg-4">
                    <h2>Travel with us</h2>
                    <p>In hac habitasse platea dictumst. Duis ac neque ut nunc varius viverra. Nullam sed pulvinar nisl, pellentesque facilisis mauris.</p>
                    <p><a class="btn btn-primary" href="#" role="button">View details »</a></p>
                </div>
            </div>
        </div>
    );
};

export default About;