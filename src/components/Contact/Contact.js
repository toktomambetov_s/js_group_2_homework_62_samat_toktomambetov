import React from 'react';
import './Contact.css';

const Contact = props => {
    return (
        <div className="Contact">
            <div className="container">
                <form>
                    <div className="form-group row">
                        <label  className="col-sm-2 col-form-label col-form-label-lg">Name</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control form-control-lg" id="lgFormGroupInput" placeholder="Name"/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label  className="col-sm-2 col-form-label col-form-label-lg">Email</label>
                        <div className="col-sm-10">
                            <input type="email" className="form-control form-control-lg" id="lgFormGroupInput" placeholder="you@example.com"/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label  className="col-sm-2 col-form-label col-form-label-lg">Phone</label>
                        <div className="col-sm-10">
                            <input type="number" className="form-control form-control-lg" id="lgFormGroupInput" placeholder="Phone number"/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label  className="col-sm-2 col-form-label col-form-label-lg">Message</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control form-control-lg" id="lgFormGroupInput" placeholder="Message"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
};

export default Contact;